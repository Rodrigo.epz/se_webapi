﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Constants
{
    public static class GlobalConstants
    {

        public const int CreatedStatusCode = 202;
        public const int InternalServerErrorCode = 500;
        public const int SqlExceptionNumberConstraint = 547;
        public const int SqlExceptionNumberDuplicateKey = 2601;
        public const int DeletedStatusCode = 204;
        public const int InvalidPackage = 400;
        public const int PackageVersionAlreadyExists = 409;
        public const string JobQueueTopic = "axcnnct-integrationjobs";
        public const string JobQueueConnectionStringSecretName = "AvidConnect-ESBConnectionString";
        public const string InValidPackage = "The binary contents of the files doesnt have valid details";

        public const string XIsNull = "{0} is null.";
        public const string XIsEmpty = "{0} is empty.";
        public const string XMustBeSpecified = "{0} must be specified.";
        public const string ToDateMustBeBiggerThanFromDate = "ToDate must be later than FromDate.";
        public const string PageSizeAndPageNumberMustBePositiveIntegers = "PageNumber and PageSize must have positive integer values.";
        public const string NoXFoundForGivenCriteria = "No {0} found for given search criteria.";
        public const string NoRunTimePackageFound = "No Package found.";
        public const string NoXFoundForGivenY = "No {0} found for given {1}.";
        public const string NoXFoundWithIdY = "No {0} found with Id {1}.";
        public const string NoXFoundWithIdYForCustomerZ = "No {0} found with Id {1} for Customer {2}.";
        public const string XMustBeGreaterThanY = "{0} must be greater than {1}.";
        public const string SqlConstraintViolation = "Saving changes failed due to a constraint violation. This is commonly the result of an invalid value for an Id property. See log for additional details.";
        public const string CustomerIdXDidNotMatchWithY = "Customer Id {0} did not match with {1}.";
        public const string CustomerIdXDidNotMatchWithYHavingIdZ = "Customer Id {0} did not match with {1} having Id {2}.";
        public const string NoActiveCustomerFoundWithIdX = "No active Customer found with an Id {0}";
        public const string SqlCannotInsertDuplicateKey = "Cannot insert duplicate record.  See log for additional details.";
        public const string DuplicateSearchCriteria = "Duplicate field names found in search criteria.";
        public const string NoXFoundForY = "No {0} found for {1}.";
        public const string XMustBeSpecifiedAndGreaterThanY = "{0} must be specified and greater than {1}.";
        public const string ImageNotInProperFormat = "Image not in proper image format. Only .jpg, .gif and .png image formats are allowed.";
        public const string NoActiveConnectorFoundForRegIdX = "No active Connector found for Registration Id {0}";
        public const string XCannotBeY = "{0} cannot be {1}.";
        public const string InvalidHostValue = "The host value cannot be {0} for {1} settings";

        public const string MD5HashNotMatched = "MD5 hash value not matched.";
        public const string VersionExists = "Package of the same version has already been uploaded.";
        public const string InvalidExecution = "An Invalid Execution exists with Id {0}";
        public const string InvalidRegistration = "An Invalid Registration Id {0}";

        public const string LatestVersionAlreadyAvailable = "The requester already has an up-to-date version.";
    }

}
