﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Exceptions
{
    public class DuplicateEntityException : System.Exception
    {
        public DuplicateEntityException(string message) : base(message)
        {

        }
    }
}
