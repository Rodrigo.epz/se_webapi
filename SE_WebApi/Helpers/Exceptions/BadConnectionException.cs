﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Exceptions
{
    public class BadConnectionException : System.Exception
    {
        public BadConnectionException(string message) : base(message)
        {

        }
    }
}
