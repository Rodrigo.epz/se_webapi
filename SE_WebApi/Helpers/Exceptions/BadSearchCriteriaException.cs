﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Exceptions
{
    public class BadSearchCriteriaException : System.Exception
    {
        public BadSearchCriteriaException(string message) : base(message)
        {


        }
    }
}
