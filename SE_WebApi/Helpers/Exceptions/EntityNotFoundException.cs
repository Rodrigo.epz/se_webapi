﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Exceptions
{
    public class EntityNotFoundException : System.Exception
    {
        public EntityNotFoundException(string message)
            : base(message)
        {
        }
    }
}
