﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Helpers.Extensions
{
    public static class ExceptionExtensions
    {
        public static Exception GetInnermostException(this Exception exception)
        {
            var cur = exception;
            while (cur?.InnerException != null)
                cur = cur?.InnerException;
            return cur;
        }
    }
}
