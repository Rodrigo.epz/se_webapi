﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SE_WebApi.Services.Interfaces;

namespace SE_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TableController : APIBaseController
    {
        ITableService _tableService;
        public TableController(ILogger<APIBaseController> logger, IHttpContextAccessor httpContextAccessor, ITableService tableService)
            : base (logger, httpContextAccessor)
        { 
            _tableService = tableService;
        }

        // GET: api/Table/getTableByHash
        [HttpGet]
        [Route("getTableByHash/{hashTable}")]
        public async Task<IActionResult> GetTableByHash(string hashTable)
        {
            try
            {
                var response = await _tableService.GetTableByHash(hashTable);
                if (response != null)
                {
                    return Ok(response);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }

}
