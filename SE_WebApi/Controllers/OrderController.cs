﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SE_WebApi.Models.WebAppOrders.Schemas;
using SE_WebApi.Services.Interfaces;

namespace SE_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : APIBaseController
    {
        IOrderService _orderService;
        public OrderController(ILogger<APIBaseController> logger, IHttpContextAccessor httpContextAccessor, IOrderService orderService)
            : base (logger, httpContextAccessor)
        {
            _orderService = orderService;
        }
        // GET: api/Order/getActiveOrders
        [HttpGet]
        [Route("getActiveOrders")]
        public async Task<IActionResult> GetActiveOrders()
        {
            try
            {
                var response = await _orderService.GetActiveOrders();
                if (response != null)
                {
                    return Ok(response);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Order/reqNewOrder
        [HttpPost]
        [Route("reqNewOrder")]
        public async Task<IActionResult> RequestNewOrder([FromBody] FoodToOrderSchema reqFoods)
        {
            try
            {
                var response = await _orderService.RequestNewOrder(reqFoods);
                if (response != null)
                {
                    return Ok(response);
                }
                return BadRequest("Hubo un problema con el identificador de su mesa");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Order/reqNewOrder
        [HttpGet]
        [Route("setOrderDespached/{id}")]
        public async Task<IActionResult> SetOrderDespached(int id)
        {
            try
            {
                var response = await _orderService.SetOrderDespached(id);
                if (response)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Order/getDetailOrder
        [HttpGet]
        [Route("getDetailOrder/{id}")]
        public async Task<IActionResult> GetDetailOrder(int id)
        {
            try
            {
                var response = await _orderService.GetDetailOrder(id);
                if (response != null)
                {
                    return Ok(response);
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Order/getDetailOrder
        [HttpGet]
        [Route("callWaitress/{id}")]
        public async Task<IActionResult> CallWaitress(int id)
        {
            try
            {
                var response = await _orderService.CallWaitress(id);
                if (response)
                {
                    return Ok();
                }
                return NotFound("La cuenta ya esta cerrada :)");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }



    }

}
