﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using SE_WebApi.Helpers.Constants;
using SE_WebApi.Helpers.Exceptions;
using SE_WebApi.Helpers.Extensions;
using SE_WebApi.Models.WebAppOrders;

namespace SE_WebApi.Controllers
{
    public class APIBaseController : Controller
    {
        protected readonly ILogger Logger;
        protected readonly DateTime DefaultFromDate = new DateTime(2018, 1, 1);
        protected readonly DateTime DefaultToDate = new DateTime(2100, 1, 1);
        protected IHttpContextAccessor _httpContextAccessor;

        public APIBaseController(ILogger<APIBaseController> logger, IHttpContextAccessor httpContextAccessor)
        {
            Logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState
                    .Where(x => x.Value.Errors.Count > 0)
                    .SelectMany(y => y.Value.Errors)
                    .ToList();

                var message = ConsolidateErrors(errors);
                context.Result = new BadRequestObjectResult(BuildApiException(message));
            }
            base.OnActionExecuting(context);
        }

        private string ConsolidateErrors(List<ModelError> errors)
        {
            var result = new StringBuilder();

            foreach (var error in errors)
            {
                var separator = (result.Length > 0) ? " | " : "";

                if (!string.IsNullOrWhiteSpace(error.ErrorMessage))
                {
                    result.Append($"{separator}{error.ErrorMessage}");
                }
                else if (!string.IsNullOrWhiteSpace(error.Exception?.Message))
                {
                    result.Append($"{separator}{error.Exception.Message}");
                }
            }

            return result.ToString();
        }

        protected ObjectResult HandleException(Exception exception)
        {
            switch (exception)
            {
                case EntityNotFoundException _: return NotFound(BuildApiException(exception));
                case BadSearchCriteriaException _:
                case FormatException _:
                    return BadRequest(BuildApiException(exception));
                case DuplicateEntityException _: return Conflict(BuildApiException(exception));
                case SqlException sqlException: return HandleException(sqlException);
            }

            var innerException = exception.GetInnermostException();
            if (innerException is SqlException sqlInnerException)
            {
                return HandleException(sqlInnerException);
            }

            return StatusCode(GlobalConstants.InternalServerErrorCode, BuildApiException(exception));
        }

        protected ObjectResult HandleException(SqlException exception)
        {
            switch (exception.Number)
            {
                case GlobalConstants.SqlExceptionNumberConstraint:
                    {
                        return BadRequest(BuildApiException(GlobalConstants.SqlConstraintViolation, exception.Message));
                    }
                case GlobalConstants.SqlExceptionNumberDuplicateKey:
                    {
                        return Conflict(BuildApiException(GlobalConstants.SqlCannotInsertDuplicateKey, exception.Message));
                    }
                default:
                    {
                        return StatusCode(GlobalConstants.InternalServerErrorCode, BuildApiException(exception));
                    }
            }
        }

        protected ApiExceptionResponseBody BuildApiException(string externalMessage, string internalMessage = null)
        {
            var response = new ApiExceptionResponseBody(externalMessage);
            var messageId = response.GetMessageId();

            Logger.LogError($"MessageId: {messageId} - ExternalErrorMessage: {externalMessage}");
            if (internalMessage != null)
            {
                Logger.LogError($"MessageId: {messageId} - InternalErrorMessage: {internalMessage}");
            }
            return response;
        }

        protected ApiExceptionResponseBody BuildApiException(Exception exception)
        {
            var response = new ApiExceptionResponseBody(exception);
            var messageId = response.GetMessageId();

            Logger.LogError($"MessageId: {messageId} - {exception.Message}");
            return response;
        }

        protected string GetUserName()
        {
            if (_httpContextAccessor.HttpContext == null) return null;

            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (string.IsNullOrEmpty(userName))
            {
                var appId = HttpContext.User.Claims
                    .FirstOrDefault(x => string.Equals(x.Type, "appid", StringComparison.InvariantCultureIgnoreCase));
                userName = appId?.Value;
            }
            return userName;
        }

        //protected string GetExtensionFromContentType(string contentType)
        //{
        //    try
        //    {
        //        return MimeTypeMap.GetExtension(contentType);
        //    }
        //    catch (Exception)
        //    {
        //        string extn = ".unknown";

        //        if (string.Equals(contentType, @"application/zip", StringComparison.InvariantCultureIgnoreCase))
        //            extn = ".zip";
        //        else if (string.Equals(contentType, @"application/xslt+xml", StringComparison.InvariantCultureIgnoreCase))
        //            extn = ".xslt";

        //        return extn;
        //    }
        //}
    }

}
