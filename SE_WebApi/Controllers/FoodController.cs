﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SE_WebApi.Services.Interfaces;

namespace SE_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : APIBaseController
    {
        IFoodService _foodService;
        public FoodController(ILogger<APIBaseController> logger, IHttpContextAccessor httpContextAccessor, IFoodService foodService)
            : base(logger, httpContextAccessor)
        {
            _foodService = foodService;
        }

        // GET: api/Food/getMenus
        [HttpGet]
        [Route("getMenus")]
        public async Task<IActionResult> GetMenus()
        {
            try
            {
                var response = await _foodService.GetMenus();
                if (response != null)
                {
                    return Ok(response);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
