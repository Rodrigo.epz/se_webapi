﻿using SE_WebApi.Models.WebAppOrders.Schemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Services.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        /// Obtiene las ordenes activas
        /// </summary>
        /// <returns></returns>
        Task<List<OrderToRenderSchema>> GetActiveOrders();

        /// <summary>
        /// Pide una orden con base a una lista de comidas
        /// </summary>
        /// <returns></returns>
        Task<OrderToWebAppSchema> RequestNewOrder(FoodToOrderSchema reqFoods);

        /// <summary>
        /// Despacha una orden
        /// </summary>
        /// <returns></returns>
        Task<bool> SetOrderDespached(int id);

        /// <summary>
        /// Obtiene el detalle de una orden
        /// </summary>
        /// <returns></returns>
        Task<FoodsDetailSchema> GetDetailOrder(int OrderId);

        /// <summary>
        /// Se llama al mesero y se cierra la orden
        /// </summary>
        /// <returns></returns>
        Task<bool> CallWaitress(int OrderId);
    }
}
