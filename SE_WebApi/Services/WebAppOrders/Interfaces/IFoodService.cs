﻿using SE_WebApi.Models.WebAppOrders.Schemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Services.Interfaces
{
    public interface IFoodService
    {
        /// <summary>
        /// Obtiene las comidas pendientes de entregar por orden por tipo
        /// </summary>
        /// <returns></returns>
        Task<FoodCategorizedSchema> GetFoodNonDispatchedByOrder(int OrderId);

        /// <summary>
        /// Obtiene todas las comidas disponibles por tipo
        /// </summary>
        /// <returns></returns>
        Task<FoodCategorizedSchema> GetMenus();
    }
}
