﻿using SE_WebApi.Models.WebAppOrders.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Services.Interfaces
{
    public interface ITableService
    {
        Task<Table> GetTableByHash(string hashTable);
    }
}
