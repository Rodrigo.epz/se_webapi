﻿using Microsoft.EntityFrameworkCore;
using SE_WebApi.Models.WebAppOrders.DB;
using SE_WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Services.WebAppOrders
{
    public class TableService : ITableService
    {
        DBContextOrders _context;

        public TableService(DBContextOrders context)
        {
            _context = context;
        }

        public async Task<Table> GetTableByHash(string hashTable)
        {
            return await _context.Tables.FirstOrDefaultAsync(tbl => tbl.HashId == hashTable);
        }
    }
}
