﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SE_WebApi.Models.WebAppOrders.DB;
using SE_WebApi.Models.WebAppOrders.Schemas;
using SE_WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SE_WebApi.Services.WebAppOrders
{
    public class OrderService : IOrderService
    {
        private static Random random = new Random();
        HttpClient _client;
        DBContextOrders _context;
        IFoodService _foodService;
        IConfiguration _config;

        public OrderService(DBContextOrders context, IFoodService foodService, IConfiguration config)
        {
            _context = context;
            _foodService = foodService;
            _client = new HttpClient
            {
                BaseAddress = new Uri($"http://192.168.1.5:3000")
            };
            _config = config;
        }
        public async Task<List<OrderToRenderSchema>> GetActiveOrders()
        {
            MapperConfiguration mapper = new MapperConfiguration(cfg => {
                cfg.CreateMap<Order, OrderToRenderSchema>();
            });
            IMapper iMapper = mapper.CreateMapper();
            var activeOrders = await _context.Orders.Where(order => order.StatusOrder.GetHashCode() < 3).ToListAsync();
            var activeOrdersSchema = iMapper.Map<List<Order>, List<OrderToRenderSchema>>(activeOrders);
            int index = 0;
            foreach (var activerOrder in activeOrdersSchema)
            {
                activeOrdersSchema[index].Food = await _foodService.GetFoodNonDispatchedByOrder(activerOrder.Id);
                index++;
            }

            return activeOrdersSchema;
        }

        public async Task<OrderToWebAppSchema> RequestNewOrder(FoodToOrderSchema requestFoods)
        {
            var tableExists = await _context.Tables.FirstOrDefaultAsync(table => table.Id == requestFoods.TableId && !table.isDeleted);
            if (tableExists != null)
            {
                MapperConfiguration mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<Order, OrderToWebAppSchema>();
                    cfg.CreateMap<Account, AccountSchema>();
                });
                IMapper iMapper = mapper.CreateMapper();

                var orderExist = await _context.Orders.Include(order => order.Account).FirstOrDefaultAsync(order => order.TableId == tableExists.Id && order.StatusOrder.GetHashCode() < 4 && !order.IsDeleted);
                if (orderExist != null)//La orden ya existe
                {
                    orderExist.OrderDate = DateTime.UtcNow;
                    orderExist.StatusOrder = StatusOrder.En_proceso;
                    _context.Update(orderExist);
                    await _context.SaveChangesAsync();

                    foreach (var reqfood in requestFoods.Foods)
                    {
                        var foodBD = await _context.Foods.FirstOrDefaultAsync(food => food.Id == reqfood.Id && !food.isDeleted);
                        if (foodBD != null)
                        {
                            while (reqfood.Qty > 0)
                            {
                                var foodOrderRelation = new OrderFood()
                                {
                                    FoodId = foodBD.Id,
                                    OrderId = orderExist.Id
                                };
                                orderExist.Account.Subtotal += foodBD.Cost;
                                _context.Update(orderExist.Account);
                                _context.Add(foodOrderRelation);
                                reqfood.Qty--;
                                await _context.SaveChangesAsync();
                            }

                        }
                    }
                    var orderToWebApp = iMapper.Map<Order, OrderToWebAppSchema>(orderExist);
                    orderToWebApp.ArrivalDate = orderExist.ArrivalDate.ToLocalTime().ToString("dd'/'MM'/'yyyy HH:mm");  //Pendiente hacer esto con el automapper u configuracion desde el startup
                    orderToWebApp.OrderDate = orderExist.OrderDate.Value.ToLocalTime().ToString("dd'/'MM'/'yyyy HH:mm");
                    orderToWebApp.Account = iMapper.Map<Account, AccountSchema>(orderExist.Account);

                    string requestUrl = $"addNewOrder";

                    var renderOrdersResponse = await _client.GetAsync(requestUrl);
                    if (renderOrdersResponse != null && renderOrdersResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        orderToWebApp.Message = "Se ha recibido la orden";
                    }
                    else
                    {
                        orderToWebApp.Message = "Su pedido esta en camino";
                    }

                    return orderToWebApp;

                }
                else
                {//Se genera una nueva cuenta y orden
                    var newAccount = new Account();//Pendiente escojer el tipo de pago
                    _context.Add(newAccount);
                    await _context.SaveChangesAsync();

                    var arrivalDate = DateTime.ParseExact(requestFoods.ArrivalDate, "dd/MM/yyyy HH:mm", null);
                    var newOrder = new Order()
                    {
                        HashNumber = RandomString(10),
                        TableId = requestFoods.TableId,
                        ArrivalDate = arrivalDate.ToUniversalTime(),
                        OrderDate = DateTime.UtcNow,
                        AccountId = newAccount.Id,
                        StatusOrder = StatusOrder.En_proceso,
                    };
                    _context.Add(newOrder);
                    await _context.SaveChangesAsync();

                    foreach (var reqfood in requestFoods.Foods)
                    {
                        var foodBD = await _context.Foods.FirstOrDefaultAsync(food => food.Id == reqfood.Id && !food.isDeleted);
                        if (foodBD != null)
                        {
                            while (reqfood.Qty > 0)
                            {
                                var foodOrderRelation = new OrderFood()
                                {
                                    FoodId = foodBD.Id,
                                    OrderId = newOrder.Id
                                };
                                newAccount.Subtotal += foodBD.Cost;
                                _context.Update(newAccount);
                                _context.Add(foodOrderRelation);
                                reqfood.Qty--;
                                await _context.SaveChangesAsync();
                            }

                        }
                    }

                    var orderToWebApp = iMapper.Map<Order, OrderToWebAppSchema>(newOrder);
                    orderToWebApp.ArrivalDate = newOrder.ArrivalDate.ToLocalTime().ToString("dd'/'MM'/'yyyy HH:mm");  //Pendiente hacer esto con el automapper u configuracion desde el startup
                    orderToWebApp.OrderDate = newOrder.OrderDate.Value.ToLocalTime().ToString("dd'/'MM'/'yyyy HH:mm");
                    orderToWebApp.Account = iMapper.Map<Account, AccountSchema>(newAccount);

                    string requestUrl = $"addNewOrder";

                    var renderOrdersResponse = await _client.GetAsync(requestUrl);
                    if (renderOrdersResponse != null && renderOrdersResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        orderToWebApp.Message = "Se ha recibido la orden";
                    }
                    else
                    {
                        orderToWebApp.Message = "Su pedido esta en camino";
                    }
                    return orderToWebApp;
                }
            }
            return null;
        }

        public async Task<bool> SetOrderDespached(int OrderId)
        {
            var orderExists = await _context.Orders.FirstOrDefaultAsync(order => order.Id == OrderId && !order.IsDeleted);
            if (orderExists != null)
            {
                orderExists.DispatchDate = DateTime.UtcNow;
                orderExists.StatusOrder = StatusOrder.Entregada;
                var orderFoodsNonDispatched = await _context.OrderFoods.Where(order => order.OrderId == orderExists.Id && !order.isDispatched && !order.isDeleted).ToListAsync();
                foreach (var orderFood in orderFoodsNonDispatched)
                {
                    orderFood.isDispatched = true;
                    _context.Update(orderFood);
                    await _context.SaveChangesAsync();
                }
                return true;
            }
            return false;
        }

        public async Task<FoodsDetailSchema> GetDetailOrder(int OrderId)
        {
            var foodsOrders = await _context.OrderFoods.Include(foodOrder => foodOrder.Food).Where(orderFood => orderFood.OrderId == OrderId && !orderFood.isDeleted).ToListAsync();
            var foodDetail = new FoodsDetailSchema() { OrderId = OrderId };
            foreach (var foodOrder in foodsOrders)
            {
                foodDetail.Foods.Add(new FoodSchema()
                {
                    Id = foodOrder.Food.Id,
                    Name = foodOrder.Food.Name,
                    Description = foodOrder.Food.Description,
                    Cost = foodOrder.Food.Cost,
                });
            }
            return foodDetail;
        }

        public async Task<bool> CallWaitress(int OrderId)
        {
            var orderExists = await _context.Orders.Include(order => order.Account).SingleOrDefaultAsync(order => order.Id == OrderId && !order.IsDeleted);
            if (orderExists != null)
            {
                orderExists.StatusOrder = StatusOrder.Cerrada;
                orderExists.IsDeleted = true;
                orderExists.DepartureDate = DateTime.UtcNow;
                _context.Update(orderExists);

                // queda pendiente el manejo de la cuenta cuando se cierra la orden
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }



        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }

}
