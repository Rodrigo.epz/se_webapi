﻿using Microsoft.EntityFrameworkCore;
using SE_WebApi.Models.WebAppOrders.DB;
using SE_WebApi.Models.WebAppOrders.Schemas;
using SE_WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Services.WebAppOrders
{
    public class FoodService : IFoodService
    {
        DBContextOrders _context;
        public FoodService(DBContextOrders context)
        {
            _context = context;
        }
        public async Task<FoodCategorizedSchema> GetFoodNonDispatchedByOrder(int OrderId)
        {
            var foodNonDispatched = new FoodCategorizedSchema();
            var ordersFoods = await _context.OrderFoods.Where(order => order.OrderId == OrderId && !order.isDispatched && !order.isDeleted).Include(order => order.Food).ToListAsync();
            foreach (var orderFood in ordersFoods)
            {
                if (orderFood.Food.FoodType == FoodType.entries)
                {
                    foodNonDispatched.Entries.Add(new FoodSchema()
                    {
                        Id = orderFood.FoodId,
                        Name = orderFood.Food.Name,
                        Description = orderFood.Food.Description,
                        Cost = orderFood.Food.Cost
                    });
                }
                if (orderFood.Food.FoodType == FoodType.mains)
                {
                    foodNonDispatched.Mains.Add(new FoodSchema()
                    {
                        Id = orderFood.FoodId,
                        Name = orderFood.Food.Name,
                        Description = orderFood.Food.Description,
                        Cost = orderFood.Food.Cost
                    });
                }
                if (orderFood.Food.FoodType == FoodType.desserts)
                {
                    foodNonDispatched.Desserts.Add(new FoodSchema()
                    {
                        Id = orderFood.FoodId,
                        Name = orderFood.Food.Name,
                        Description = orderFood.Food.Description,
                        Cost = orderFood.Food.Cost
                    });
                }
                if (orderFood.Food.FoodType == FoodType.drinks)
                {
                    foodNonDispatched.Drinks.Add(new FoodSchema()
                    {
                        Id = orderFood.FoodId,
                        Name = orderFood.Food.Name,
                        Description = orderFood.Food.Description,
                        Cost = orderFood.Food.Cost
                    });
                }
            }
            return foodNonDispatched;
        }

        public async Task<FoodCategorizedSchema> GetMenus()
        {
            var foodsCategorized = new FoodCategorizedSchema();
            var foods = await _context.Foods.Where(food => !food.isDeleted).ToListAsync();
            foreach (var food in foods)
            {
                if (food.FoodType == FoodType.entries)
                {
                    foodsCategorized.Entries.Add(new FoodSchema()
                    {
                        Id = food.Id,
                        Name = food.Name,
                        Description = food.Description,
                        Cost = food.Cost
                    });
                }
                if (food.FoodType == FoodType.mains)
                {
                    foodsCategorized.Mains.Add(new FoodSchema()
                    {
                        Id = food.Id,
                        Name = food.Name,
                        Description = food.Description,
                        Cost = food.Cost
                    });
                }
                if (food.FoodType == FoodType.desserts)
                {
                    foodsCategorized.Desserts.Add(new FoodSchema()
                    {
                        Id = food.Id,
                        Name = food.Name,
                        Description = food.Description,
                        Cost = food.Cost
                    });
                }
                if (food.FoodType == FoodType.drinks)
                {
                    foodsCategorized.Drinks.Add(new FoodSchema()
                    {
                        Id = food.Id,
                        Name = food.Name,
                        Description = food.Description,
                        Cost = food.Cost
                    });
                }
            }
            return foodsCategorized;
        }

    }

}
