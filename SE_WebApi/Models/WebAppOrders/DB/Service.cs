﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class Service : BaseEntity
    {
        public string Ticket { get; set; }
        public string ServiceStatus { get; set; }
        public string Empleado { get; set; }
        public bool isDeleted { get; set; }
    }
}
