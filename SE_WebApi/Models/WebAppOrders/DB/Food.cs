﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class Food : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public FoodType FoodType { get; set; }
        public bool isDeleted { get; set; }

        public ICollection<OrderFood> OrdersFoods { get; set; }
    }

    public enum FoodType { entries = 1, mains, desserts, drinks}
}
