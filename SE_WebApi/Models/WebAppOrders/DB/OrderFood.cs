﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class OrderFood : BaseEntity
    {
        public int OrderId { get; set; }
        public int FoodId { get; set; }
        public bool isDispatched { get; set; } = false;
        public bool isDeleted { get; set; } = false;

        public Order Order { get; set; }
        public Food Food { get; set; }
    }
}
