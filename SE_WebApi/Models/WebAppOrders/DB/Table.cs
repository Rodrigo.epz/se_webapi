﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class Table : BaseEntity
    {
        public string Description{ get; set; }
        public string HashId { get; set; }
        public bool isDeleted { get; set; }
    }
}
