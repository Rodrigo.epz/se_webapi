﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class DBContextOrders : DbContext
    {
        IConfiguration _configuration;
        //public DBContextOrders()
        //{
        //}
        public DBContextOrders(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        //public DBContextOrders(DbContextOptions<DBContextOrders> options) : base(options)
        //{

        //}


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("Local"));
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //}

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderFood> OrderFoods { get; set; }
        public DbSet<Table> Tables { get; set; }
    }
}
