﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
