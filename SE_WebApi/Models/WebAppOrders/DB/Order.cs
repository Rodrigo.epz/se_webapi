﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class Order : BaseEntity
    {
        public string Ticket { get; set; }
        public string HashNumber { get; set; }
        public int TableId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DispatchDate { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? EstimatedDelivaryDate { get; set; }
        public int AccountId { get; set; }
        public StatusOrder StatusOrder { get; set; }
        public bool IsDeleted { get; set; } = false;

        public Table Table { get; set; }
        public Account Account { get; set; }
        public ICollection<OrderFood> OrdersFoods { get; set; }
    }

    public enum StatusOrder { Espera = 1, En_proceso, Entregada, Cerrada }
}
