﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.DB
{
    public class Account : BaseEntity
    {
        public PaymentType PaymentType { get; set; } = PaymentType.Efectivo;
        public double Subtotal { get; set; } = 0;
        public double Tip { get; set; } = 0;
        public double Total { get; set; } = 0;
        public bool isDeleted { get; set; } = false;
    }

    public enum PaymentType { Efectivo = 1, Tarjeta_Debito, Tarjeta_Credito, Paypal }
}
