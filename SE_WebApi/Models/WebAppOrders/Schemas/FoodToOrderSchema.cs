﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.Schemas
{
    public class FoodToOrderSchema
    {
        public FoodToOrderSchema()
        {
            Foods = new List<FoodByTableSchema>();
        }
        public int TableId { get; set; }
        public string ArrivalDate { get; set; }
        public List<FoodByTableSchema> Foods { get; set; }
    }

    public class FoodByTableSchema
    {
        public int Id { get; set; }
        public int Qty { get; set; }
    }
}
