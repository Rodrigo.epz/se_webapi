﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.Schemas
{
    public class OrderToRenderSchema
    {
        public int Id { get; set; }
        public string Ticket { get; set; }
        public string HashNumber { get; set; }
        public int TableId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime OrderDate { get; set; }
        public FoodCategorizedSchema Food { get; set; }
        public string StatusOrder { get; set; }
    }
}
