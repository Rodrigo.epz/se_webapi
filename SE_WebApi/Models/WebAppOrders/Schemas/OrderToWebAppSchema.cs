﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SE_WebApi.Models.WebAppOrders.Schemas
{
    public class OrderToWebAppSchema
    {
        public int Id { get; set; }
        public string Ticket { get; set; }
        public string HashNumber { get; set; }
        public string ArrivalDate { get; set; }
        public string OrderDate { get; set; }
        public string DispatchDate { get; set; }
        public string DepartureDate { get; set; }
        public string EstimatedDelivaryDate { get; set; }
        public AccountSchema Account { get; set; }
        public string StatusOrder { get; set; }
        public string Message { get; set; }
    }
    public class AccountSchema
    {
        public int Id { get; set; }
        public string PaymentType { get; set; }
        public double Subtotal { get; set; }
        public double Tip { get; set; }
        public double Total { get; set; }
    }
}
