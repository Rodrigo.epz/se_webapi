﻿using System.Collections.Generic;

namespace SE_WebApi.Models.WebAppOrders.Schemas
{
    public class FoodCategorizedSchema
    {
        public FoodCategorizedSchema()
        {
            Entries = new List<FoodSchema>();
            Mains = new List<FoodSchema>();
            Desserts = new List<FoodSchema>();
            Drinks = new List<FoodSchema>();
        }
        public ICollection<FoodSchema> Entries { get; set; }
        public ICollection<FoodSchema> Mains { get; set; }
        public ICollection<FoodSchema> Desserts { get; set; }
        public ICollection<FoodSchema> Drinks { get; set; }
    }

    public class FoodSchema
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
    }

    public class FoodsDetailSchema
    {
        public FoodsDetailSchema()
        {
            Foods = new List<FoodSchema>();
        }
        public int OrderId { get; set; }
        public List<FoodSchema> Foods { get; set; }
    }
}
